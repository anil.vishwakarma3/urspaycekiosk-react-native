import {
    GUEST_ID,
    SET_LANGUAGE_SUCCESS,
    LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_FAILURE,
} from './action-types';
import axiosInstance from '../../axios/axiosinstance';
import { GOOGLE_API_KEY } from '../../constants';
import axios from 'axios';
// import messaging from '@react-native-firebase/messaging';


// Get Online Payment Details
export function checkPaymentStatus(data) {
    return async dispatch => {
        try {
            var response = await axiosInstance.get(`https://api.tap.company/v2/charges/${data}`, {
                headers: { "Authorization": "Bearer sk_test_2yZFHpzmMiGNoIguKCPTvSR9" }
            })
            var responseJson = response.data;
            if (responseJson?.status) { return Promise.resolve(responseJson); }
            else { return Promise.reject(responseJson) }
        }
        catch (e) {
            console.log("catch error API checkPaymentStatus", e)
            return Promise.reject(e)
        }
    }
}