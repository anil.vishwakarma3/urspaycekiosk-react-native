import { Platform } from 'react-native';
import {
    CONTROL_REFRESER,
    LOGOUT_REQUEST, LOGOUT_SUCCESS, LOGOUT_FAILURE,
    SIGN_UP_REQUEST, SIGN_UP_SUCCESS, SIGN_UP_FAILURE,
    SIGN_IN_REQUEST, SIGN_IN_SUCCESS, SIGN_IN_FAILURE,
    GET_USER_ADDRESS_LIST_REQUEST, GET_USER_ADDRESS_LIST_SUCCESS, GET_USER_ADDRESS_LIST_FAILURE,
    GET_USER_PROFILE_REQUEST, GET_USER_PROFILE_SUCCESS, GET_USER_PROFILE_FAILURE,
    SET_USER_DEFAULT_ADDRESS, DELETE_USER_DEFAULT_ADDRESS,
    GET_USER_CARD_LIST_REQUEST, GET_USER_CARD_LIST_SUCCESS, GET_USER_CARD_LIST_FAILURE,
    GET_ORDER_LIST_REQUEST, GET_ORDER_LIST_SUCCESS, GET_ORDER_LIST_FAILURE, GUEST_ID,
    GET_CART_LIST_REQUEST, GET_CART_LIST_SUCCESS, GET_CART_LIST_FAILURE,
    GET_TIME_LIST_REQUEST, GET_TIME_LIST_SUCCESS, GET_TIME_LIST_FAILURE,
    GET_PRICE_LIST_REQUEST, GET_PRICE_LIST_SUCCESS, GET_PRICE_LIST_FAILURE,

} from '../../actions/action-types';


const initialState = {
    deviceToken: '', deviceType: Platform?.OS?.toLocaleLowerCase() || '', loginToken: '',
    userData: null, profileData: null, SignIn: false,
    signin_Loader: false,
    GuestId: null,
    userAddressList: [], userDefaultAddress: null,
    userPaymentCardList: [],
    userOrderList: [],
    userCartList: [], cartCount: 0,
    timeList: [],
    priceList: [],


}
export const authReducer = (state = initialState, { type, payload }) => {
    switch (type) {

        //Guest
        case GUEST_ID:
            return {
                ...state, GuestId: payload || null, loginToken: '', userData: null, profileData: null, SignIn: false,
                signin_Loader: false, userAddressList: [], userDefaultAddress: null, userPaymentCardList: [], userOrderList: [],
                userCartList: [], cartCount: 0
            }

        // For Logout
        case LOGOUT_REQUEST:
            return {
                ...state, Logout_Loader: true
            }
        case LOGOUT_SUCCESS:
            return {
                ...state, deviceToken: '', loginToken: '', userData: null, profileData: null, SignIn: false,
                signin_Loader: false, GuestId: null, userAddressList: [], userCartList: [], cartCount: 0,
                userDefaultAddress: null,
                userPaymentCardList: [], userOrderList: [],
            }
        case LOGOUT_FAILURE:
            return {
                ...state, deviceToken: '', loginToken: '', userData: null, profileData: null, SignIn: false,
                signin_Loader: false, GuestId: null, userAddressList: [], userCartList: [], cartCount: 0,
                userDefaultAddress: null,
                userPaymentCardList: [], userOrderList: [],
            }

        // For SIGN_IN
        case SIGN_IN_REQUEST:
            return {
                ...state, deviceToken: '', loginToken: '', userData: null, profileData: null, SignIn: false,
                signin_Loader: false, userAddressList: []
            };
        case SIGN_IN_SUCCESS:
            return {
                ...state, userData: payload?.response || null, SignIn: true, signin_Loader: false,
                deviceToken: '', loginToken: '', GuestId: null,
            };
        case SIGN_IN_FAILURE:
            return {
                ...state, deviceToken: '', loginToken: '', userData: null, profileData: null, SignIn: false,
                signin_Loader: false, userAddressList: []
            };



        // For GET_ORDER_LIST
        case GET_PRICE_LIST_REQUEST:
            return {
                ...state,
            };
        case GET_PRICE_LIST_SUCCESS:
            return {
                ...state, priceList: payload?.data || []
            };
        case GET_PRICE_LIST_FAILURE:
            return {
                ...state, priceList: []
            };



        default:
            return state
    }
}