import React, { PureComponent } from 'react';
import {
    View, StyleSheet, SafeAreaView, Text, Animated, Dimensions, Image, ImageBackground,
} from 'react-native';
import { Colors } from './colors';
import { Assets } from './assets';




const { width, height } = Dimensions.get('window')

class ImageLoader extends PureComponent {
    state = {
        opacity: new Animated.Value(0),
    }

    componentDidMount() {
        this.onLoad()
    }

    onLoad = () => {
        Animated.timing(this.state.opacity, {
            toValue: 1,
            duration: 2000,
            useNativeDriver: false,
        }).start();
    }

    render() {
        return (
            <>
                <Animated.View
                    {...this.props}
                    style={[{
                        opacity: this.state.opacity,
                        transform: [
                            {
                                scale: this.state.opacity.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0.1, 1],
                                }),
                            },
                        ],
                    }, this.props.style]}
                >
                    <Image source={Assets.APP_LOG} style={{
                        width: 500, height: '100%',
                        resizeMode: 'contain',
                    }} />
                </Animated.View>
            </>
        );
    }
}

const SplashScreen = ({ progress = 0.5 }) => (
    <>
        <SafeAreaView style={styles.container} >
            <View
                style={{
                    flex: 1, alignItems: 'center',
                    justifyContent: 'center',
                }}>
                <View style={styles.containerView}>
                    {/* <ImageLoader style={styles.image} /> */}
                    {/* <SVGICON.APP_LOGO_ICON /> */}
                    {/* <Image source={Assets.APP_LOGO} style={{
                        width: '100%',
                        resizeMode: 'contain',
                    }} />  */}
                    <View style={styles.containerView}>
                        <Text style={{ marginBottom: 25, color: '#000', alignSelf: 'center', fontSize: 35, letterSpacing: 1 }} >Welcome To</Text>
                        <ImageLoader style={styles.image} />
                    </View>
                </View>

            </View>
            {/* <Text style={{ color: '#000', alignSelf: 'center', fontSize: 18, paddingVertical: 15 }} >LAUNDRY</Text> */}
        </SafeAreaView>
    </>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.COLOR_WHITE,
    },
    containerView: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.COLOR_WHITE,
        marginVertical: 10,
        width: 450,
        height: 100,
        alignSelf: "center"
    },
    image: {
        width: '100%',
        // resizeMode: 'contain',
    },
    RightText: {
        textAlign: 'center',
        color: '#5d5e5d',
        fontWeight: '500',
        fontSize: 14,
        paddingHorizontal: 14,
        position: 'absolute',
        bottom: 5,
        width: '100%',
        textAlign: 'center'
    }
});

export default SplashScreen;
