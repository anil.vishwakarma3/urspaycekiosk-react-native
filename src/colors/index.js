export const Colors = {
    TRANSPARENT: 'transparent',
    APPTHEME: '#0037dc',

    THEME_BG_COLOR: '#176CFF',
    TEXT_THEME_COLOR: '#176CFF',
    TEXT_THEME: '#121212',
    TEXT_GREY: '#5A5A5A',
    COLOR_WHITE: '#fff',
    COLOR_BLACK:'#000'

}
