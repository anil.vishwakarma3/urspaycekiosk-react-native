import React, { useEffect, useState } from 'react';
import { StatusBar, Text, SafeAreaView } from 'react-native';


import SplashScreen from './splashScreen';
import { Colors } from './colors';
import { STATUS_BAR_HEIGHT } from './constants';
import AppContainer from './navigation/Index';


export default function App() {
    const [Splash, setSplash] = useState(false);
    const [progressSpeed, setProgressSpeed] = useState(0.5);

    useEffect(() => {
        onProgressChanges()
        setTimeout(() => { setSplash(true) }, 5000);
    }, [])

    const onProgressChanges = () => {
        setTimeout(() => {
            setProgressSpeed(1)
        }, 3000)
    }
    return (
        <>

            {
                !Splash
                    ?
                    <>
                        <StatusBar hidden />
                        <SplashScreen progress={progressSpeed} />
                    </>
                    : <>
                        <SafeAreaView style={{ width: '100%', height: STATUS_BAR_HEIGHT, backgroundColor: '#000' }}>
                            <StatusBar backgroundColor={'#000'} barStyle={'light-content'} />
                        </SafeAreaView>
                        <AppContainer />
                    </>
            }

        </>
    )
}