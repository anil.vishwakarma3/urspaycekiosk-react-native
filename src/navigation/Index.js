// AppStackNavigator.js
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
// import { enableScreens } from 'react-native-screens';
import Home from '../screens/Home';
// import Qrscreen from '../screens/Qrscreen';
import { Dimensions } from 'react-native';
import Visitortype from '../screens/Visitortype';
import Qrscreen from '../screens/Qrscreen';
import Welcome from '../screens/Welcome';
import Getpass from '../screens/Getpass';
import Information from '../components/Information.js/Index';
import Client from '../screens/Client';
import Picture from '../screens/Picture';
import Kyc from '../screens/Kyc';
import Signature from '../screens/Signature';
import Thank from '../screens/Thank';

// enableScreens(true);
const { width, height } = Dimensions.get('window');
const Stack = createNativeStackNavigator();

function AppContainer() {
    return (
        <NavigationContainer style={{ backgroundColor: '#fff', with: width, height: height }}>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
                <Stack.Screen name="Getpass" component={Getpass} options={{ headerShown: false }} />
                <Stack.Screen name="Welcome" component={Welcome} options={{ headerShown: false }} />
                <Stack.Screen name="Information" component={Information} options={{ headerShown: false }} />
                <Stack.Screen name="Visitortype" component={Visitortype} options={{ headerShown: false }} />
                <Stack.Screen name="Qrscreen" component={Qrscreen} options={{ headerShown: false }} />

                <Stack.Screen name="MeetWith" component={Client} options={{ headerShown: false }} />
                <Stack.Screen name="Picture" component={Picture} options={{ headerShown: false }} />
                <Stack.Screen name="Kyc" component={Kyc} options={{ headerShown: false }} />
                <Stack.Screen name="Signature" component={Signature} options={{ headerShown: false }} />
                <Stack.Screen name="Thank" component={Thank} options={{ headerShown: false }} />

            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default AppContainer;

