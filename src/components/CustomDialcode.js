import React, { useState } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

const Dailcode = () => {
    const dialCodes = ['+1', '+91', '+44', '+61']; // Example list of dial codes

    const [isDropdownVisible, setIsDropdownVisible] = useState(false);
    const [selectedDialCode, setSelectedDialCode] = useState('');

    const toggleDropdown = () => {
        setIsDropdownVisible(!isDropdownVisible);
    };

    const handleDialCodeSelect = (code) => {
        setSelectedDialCode(code);
        onSelect(code); // Pass selected dial code back to parent component
        setIsDropdownVisible(false);
    };
    return (
        <View style={styles.dropdownContainer}>
            <TouchableOpacity style={styles.selectedDialCode} onPress={toggleDropdown}>
                <Text>{selectedDialCode || 'Select Code'}</Text>
            </TouchableOpacity>
            {isDropdownVisible && (
                <View style={styles.dropdown}>
                    {dialCodes.map((code) => (
                        <TouchableOpacity
                            key={code}
                            style={styles.dropdownItem}
                            onPress={() => handleDialCodeSelect(code)}
                        >
                            <Text>{code}</Text>
                        </TouchableOpacity>
                    ))}
                </View>
            )}
        </View>
    )
}
const styles = StyleSheet.create({

    input_box: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    input: {
        flex: 1,
        marginLeft: 10,
        height: 40,
        borderBottomWidth: 1,
        borderBottomColor: '#D2D2D2',
        fontSize: 16,
    },
    dropdownContainer: {
        position: 'relative',
    },
    selectedDialCode: {
        padding: 10,
        borderWidth: 1,
        borderColor: '#D2D2D2',
        borderRadius: 5,
    },
    dropdown: {
        position: 'absolute',
        top: 40,
        left: 0,
        right: 0,
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#D2D2D2',
        borderRadius: 5,
        zIndex: 1000,
    },
    dropdownItem: {
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#D2D2D2',
    },
});
export default Dailcode