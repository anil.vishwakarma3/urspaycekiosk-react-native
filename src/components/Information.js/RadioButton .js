import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Colors } from '../../colors';

const RadioButton = ({ selected, onPress }) => (
    <TouchableOpacity onPress={onPress} style={styles.radioButtonContainer}>
        <View style={[styles.radioButton, selected && styles.radioButtonSelected]}>
            {selected && <View style={styles.radioButtonInner} />}
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    radioButtonContainer: {
        marginRight: 10,
    },
    radioButton: {
        height: 20,
        width: 20,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#959AA1',
        alignItems: 'center',
        justifyContent: 'center',
    },
    radioButtonSelected: {
        borderColor: '#176CFF',
    },
    radioButtonInner: {
        height: 10,
        width: 10,
        borderRadius: 5,
        backgroundColor: '#176CFF',
    },
});

export default RadioButton;
