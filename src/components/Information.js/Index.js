import React from 'react'
import { Dimensions, ScrollView, Text, View } from 'react-native'
import Tabs from './Tabs'
import Header from '../../screens/Header'
import { useNavigation } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');
const Information = () => {
    const navigation = useNavigation();
    return (
        <View style={{ backgroundColor: '#fff', with: '100%', height: '100%' }}>
            <ScrollView style={{ paddingBottom: 10 }}>
                <Header title="Visitortype" showBackButton={true} nextScreen="MeetWith" />
                <Tabs />
            </ScrollView>
        </View>
    )
}

export default Information 