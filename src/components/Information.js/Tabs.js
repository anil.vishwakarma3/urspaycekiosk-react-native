import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, Dimensions } from 'react-native';
import { Colors } from '../../colors';
import Personalinformation from '../../screens/Personalinformation';
import RadioButton from './RadioButton ';
// import RadioButton from '../../components/RadioButton';

const { width, height } = Dimensions.get('window');
const Tabs = () => {
  const [tabs, setTabs] = useState(1);

  return (
    <View style={{ paddingHorizontal: 20, paddingTop: 50, width: '100%', height: '100%', }}>
      <View style={styles.tabs}>
        <TouchableOpacity
          style={[styles.tab, tabs === 1 && styles.activeTab]}
          onPress={() => setTabs(1)}
        >
          <RadioButton selected={tabs === 1} onPress={() => setTabs(1)} />
          <Text style={styles.tabText}>I am a new visitor</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.tab, tabs === 2 && styles.activeTab]}
          onPress={() => setTabs(2)}
        >
          <RadioButton selected={tabs === 2} onPress={() => setTabs(2)} />
          <Text style={styles.tabText}>I have QR Code</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.tab, tabs === 3 && styles.activeTab]}
          onPress={() => setTabs(3)}
        >
          <RadioButton selected={tabs === 3} onPress={() => setTabs(3)} />
          <Text style={styles.tabText}>I have Id card</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.pans_box}>
        {tabs === 1 && (
          <View>
            <Personalinformation />
          </View>
        )}
        {tabs === 2 &&
          <View style={{ width: '100%', height: height * 0.50, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={[{ color: '#000', fontSize: 20 }]} >Not found </Text>
          </View>}
        {tabs === 3 &&
          <View style={{ width: '100%', height: height * 0.50, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={[{ color: '#000', fontSize: 20 }]} >Not found </Text>
          </View>}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  tabText: {
    fontSize: 16,
    color: '#7D7D7D',
    fontWeight: '400'
  },
  pans_box: {
    width: '100%',
    paddingHorizontal: 100
  },
  tabs: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 20,
    width: '100%',
    paddingHorizontal: 120,
  },
  tab: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderBottomWidth: 2,
    borderColor: 'transparent',
  },
  activeTab: {
    // borderColor: 'red',
  },
});

export default Tabs;
