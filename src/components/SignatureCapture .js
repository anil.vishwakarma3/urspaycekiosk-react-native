import React, { useRef } from 'react';
import SignatureScreen from 'react-native-signature-canvas';

const SignatureCapture = ({ text, onOK }) => {
    const ref = useRef();

    // Called after ref.current.readSignature() reads a non-empty base64 string
    const handleOK = (signature) => {
        if (onOK) {
            onOK(signature); // Callback from Component props
        }
        console.log(signature);
    };

    // Called after ref.current.readSignature() reads an empty string
    const handleEmpty = () => {
        console.log("Empty");
    };

    // Called after ref.current.clearSignature()
    const handleClear = () => {
        console.log("clear success!");
    };

    // Called after end of stroke
    const handleEnd = () => {
        if (ref.current) {
            ref.current.readSignature();
        }
    };

    // Called after ref.current.getData()
    const handleData = (data) => {
        console.log(data);
    };

    return (
        <SignatureScreen
            ref={ref}
            onEnd={handleEnd}
            onOK={handleOK}
            onEmpty={handleEmpty}
            onClear={handleClear}
            onGetData={handleData}
            autoClear={true}
            descriptionText={text}
        />
    );
};

export default SignatureCapture;
