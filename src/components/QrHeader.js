import React, { useCallback, useEffect, useState } from 'react';
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../colors';
import { useNavigation } from '@react-navigation/native';
import { Assets } from '../assets';

const { width, height } = Dimensions.get('window');
const QrHeader = () => {
    const navigation = useNavigation();
    const Heding = "Scan and pair";
    const text_home = 'To pair this device you can scan the QR code available on';
    const add_LINK = 'Home > Visitors > Kiosk Devices > New Device'
    return (
        <View style={styles.header_style}>
            <View style={[styles.header_flex, { position: 'relative', left: 20 }]}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Image
                        style={{ }}
                        source={Assets.APP_LOG} />
                </TouchableOpacity>
            </View>
            <View style={styles.header_flex}>
                <Text style={{ paddingBottom: 5, fontWeight: '600', color: Colors.COLOR_WHITE, fontSize: 25, textAlign: 'center' }}>{Heding}</Text>
                <Text style={styles.text_style}>{text_home}</Text>
                <Text style={styles.text_style2}>{add_LINK}</Text>
            </View>
            <View style={styles.header_flex}></View>
        </View>
    )
}
const styles = StyleSheet.create({
    header_style: {
        // display: 'flex',
        // flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
        // paddingHorizontal: 100,
        paddingVertical: 20,
        // backgroundColor:'red'
        position: 'relative',
        top: 50

    },
    header_flex: {
        textAlign: "center",
    },
    text_style: {
        color: '#FFFFFFAB',
        fontSize: 12,
        fontWeight: '400',
        textAlign: 'center',
    },
    text_style2: {
        color: Colors.COLOR_WHITE,
        fontSize: 20,
        fontWeight: '500',
        textAlign: 'center',
    },
})

export default QrHeader
