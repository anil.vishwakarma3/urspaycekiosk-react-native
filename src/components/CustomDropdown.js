// CustomDropdown.js
import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, FlatList } from 'react-native';
import { Colors } from '../colors';
import Icon from 'react-native-vector-icons/MaterialIcons';

const CustomDropdown = ({ options, onSelect, placeholder, selectedValue }) => {
    const [isOpen, setIsOpen] = useState(false);

    const handleSelect = (option) => {
        onSelect(option);
        setIsOpen(false);
    };

    return (
        <View style={{ position: 'relative' }}>
            <TouchableOpacity
                style={styles.dropdown}
                onPress={() => setIsOpen(!isOpen)}
            >
                <Text style={styles.selectedText}>
                    {selectedValue ? selectedValue : placeholder}
                </Text>
                <Icon
                    // name={isOpen ? 'chevron-up' : 'chevron-down'}
                    size={24}
                    color={Colors.COLOR_WHITE}
                />
            </TouchableOpacity>
            {isOpen && (
                <View style={styles.dropdownList}>
                    <FlatList
                        data={options}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => (
                            <TouchableOpacity
                                style={styles.option}
                                onPress={() => handleSelect(item)}
                            >
                                <Text style={styles.optionText}>{item}</Text>
                            </TouchableOpacity>
                        )}
                    />
                </View>
            )}
            {/* {selectedValue && (
                <Text style={styles.selectedValue}>Selected: {selectedValue}</Text>
            )} */}
        </View>
    );
};

const styles = StyleSheet.create({
    // container: {
    //     // width: '80%',
    //     alignSelf: 'center',
    //     marginBottom: 20,
    // },
    dropdown: {
        // backgroundColor: Colors.THEME_BG_COLOR,
        padding: 5,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Colors.COLOR_GRAY,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: 300,
        paddingHorizontal: 15,
        zIndex: 9
    },
    selectedText: {
        color: Colors.COLOR_BLACK,
        fontSize: 14,
    },
    dropdownList: {
        backgroundColor: Colors.COLOR_WHITE,
        marginTop: 5,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: Colors.COLOR_GRAY,
        maxHeight: 200,
        width: 300,
        position: "absolute",
        top: 45,
        zIndex: 5
    },
    option: {
        padding: 15,
    },
    optionText: {
        fontSize: 16,
        color: Colors.COLOR_BLACK,
    },
    selectedValue: {
        marginTop: 10,
        fontSize: 16,
        color: Colors.COLOR_BLACK,
    },
});

export default CustomDropdown;
