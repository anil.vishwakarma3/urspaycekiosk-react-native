export const Assets = {
    SPLASH_SCREEN: require('./images/Arrow.png'),
    APP_LOG: require('./images/MAinLogo.png'),
    CLOSE: require('./images/close.png'),
    THNAX: require('./images/Thank.png'),
    CHECK_IN: require('./images/checkOut.png'),
    CHECK_OUT: require('./images/checkIN.png'),
};  