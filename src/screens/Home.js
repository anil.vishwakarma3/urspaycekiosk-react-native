import React, { useState } from 'react';
import { Dimensions, Image, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Colors } from '../colors';
import { useNavigation } from '@react-navigation/native';
import Header from './Header';
import Footer from './Footer';
import SvgLogo from '../assets/svg/LOGO.svg'
// import VideoPlayer from 'react-native-video-player';


const { width, height } = Dimensions.get('window');

const Home = () => {
    const navigation = useNavigation();
    const text_home = 'To pair this device you can scan the QR code available at';
    const add_LINK = 'Home > Visitors > Kiosk Devices > New Device';
    const ViedoText = 'How to pair Kiosk tablet?';
    const Scan = 'Scan and pair';

    const [vidoModal, setVidoModal] = useState(false)
    return (
        <View style={{ flex: 1, width: '100%', height: 100 }}>
            <Header title="Home" showBackButton={true} nextScreen="Getpass" />
            {/* <SvgLogo/> */}
            <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: '100%' }}>
                <View style={{ textAlign: 'center', position: 'relative', bottom: 60 }}>
                    <Text style={styles.text_style}>{text_home}</Text>
                    {/* <SvgLogo width={100} height={100} /> */}

                    <Text style={styles.text_style2}>{add_LINK}</Text>
                    <TouchableOpacity
                        style={styles.home_button}
                        onPress={() => { navigation.navigate('Qrscreen') }}
                    >
                        <Image
                            style={{ width: 60, height: 60 }}
                            source={require('../assets/images/qr_code_scanner.png')}
                        />
                        <Text style={{ paddingLeft: 30, color: Colors.COLOR_WHITE, fontWeight: '500', fontSize: 30 }}>{Scan}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.video_style}
                        onPress={() => setVidoModal(true)}
                    >
                        <Image
                            style={{ width: 40, height: 40, marginRight: 10 }}
                            source={require('../assets/images/video.png')}
                        />
                        <Text style={{ color: Colors.COLOR_BLACK, fontSize: 20, }}>{ViedoText}</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <Footer />

            <Modal
                animationType="slide"
                transparent={true}
                visible={vidoModal}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                    setVidoModal(!modalVisible);
                }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        {/* <VideoPlayer
                            video={require('../assets/video/urspayce.mp4')}
                        // videoWidth={200}
                        // videoHeight={200}
                        // thumbnail={require('../assets/images/Ellipse.png')}
                        /> */}
                        <TouchableOpacity onPress={() => setVidoModal(false)}><Text>Close</Text></TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({

    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000000ba'
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#0000f0',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    home_button: {
        backgroundColor: Colors.THEME_BG_COLOR,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        paddingVertical: 40,
        marginVertical: 60,
        paddingHorizontal: 10,
        // width:'100%'
    },
    video_style: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    text_style: {
        color: Colors.TEXT_GREY,
        fontSize: 18,
        fontWeight: '400',
        textAlign: 'center',
    },
    text_style2: {
        color: Colors.TEXT_THEME,
        fontSize: 18,
        fontWeight: '500',
        textAlign: 'center',
    },
});

export default Home;
