import React, { useState } from 'react'
import { Image, Modal, Text, TouchableOpacity, View } from 'react-native'

const Language = () => {
    const [lanModal, setLanModal] = useState(false)
    return (
        <>
            <View>
                <Image
                    style={{ width: 50, height: 50, borderRadius: 50 }}
                    source={require('../assets/images/language.png')} />
                <TouchableOpacity onPress={() => setLanModal(true)}>
                    <Text style={{ color: 'red' }}>Click</Text>
                </TouchableOpacity>

            </View>
            <Modal
                visible={lanModal}
            >
                <Text style={{ color: 'red' }}>aaaaaaaaaa</Text>
            </Modal>
        </>
    )
}

export default Language 