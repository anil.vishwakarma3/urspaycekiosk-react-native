import React, { useRef } from "react";
import { StyleSheet, View, Button, Text } from "react-native";
import SignatureScreen from "react-native-signature-canvas";
import Header from "./Header";
import { Colors } from "../colors";

const Signature = ({ onOK }) => {
    const ref = useRef();

    const handleOK = (signature) => {
        if (onOK) {
            onOK(signature);
        }
        console.log(signature);
    };

    const handleClear = () => {
        ref.current.clearSignature();
    };

    const handleConfirm = () => {
        console.log("end");
        ref.current.readSignature();
    };

    const style = `.m-signature-pad--footer {display: none; margin: 0px;}`;

    const heading = 'What is Lorem Ipsum?';
    const lorem = 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknowe specimen book. It has survived not only  ';

    return (
        <View style={{ backgroundColor: '#fff', width: '100%', height: '100%' }}>
            <Header title="Home" showBackButton={true} nextScreen="Kyc" />
            <View style={{ width: '70%', marginTop: 30, alignSelf: "center" }}>
                <Text style={{ color: Colors.COLOR_BLACK, fontSize: 22, fontWeight: '600', textAlign: "center", marginBottom: 20 }}>NDA Document</Text>
                <Text style={[styles.text, { fontWeight: '600' }]}>{heading}</Text>
                <Text style={styles.text}>{lorem}</Text>
                <Text style={[styles.text, { fontWeight: '600' }]}>{heading}</Text>
                <Text style={styles.text}>{lorem}</Text>
                {/* <Text style={styles.text}>{DPC}</Text> */}
                <View style={styles.signatureContainer}>
                    <SignatureScreen
                        ref={ref}
                        onOK={handleOK}
                        webStyle={style}
                        style={styles.signaturePad}
                    />
                </View>
                <View style={styles.row}>
                    <Text onPress={handleClear} >Signature</Text>
                    <Text style={{ color: "#DC3545" }} onPress={handleClear} >Clear</Text>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    text: {
        color: Colors.COLOR_BLACK,
        fontSize: 16,
        marginBottom: 10,
        fontWeight: '400'
    },
    signatureContainer: {
        width: '100%',
        height: 200, // Adjusting the height to 200 units
        backgroundColor: 'white', // Adding a background color to differentiate
        borderColor: 'black',
        borderBottomWidth: 1,
        marginTop: 20,
        elevation: 0
    },
    signaturePad: {
        flex: 1,
    },
    row: {
        flexDirection: "row",
        justifyContent: 'space-between',
        width: '100%',
        marginTop: 10,
    },
});

export default Signature;
