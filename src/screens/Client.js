import React, { useState } from 'react';
import { Dimensions, FlatList, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Header from './Header';
import { Colors } from '../colors';
import { useNavigation } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');

const Client = () => {
    const navigation = useNavigation();
    const [searchQuery, setSearchQuery] = useState('');
    const Heading = 'Who you want to meet with?';

    const ListClient = [
        { id: '1', name: 'Jack Simpson', boi: 'Digital marketing executive', image: require('../assets/images/Ellipse.png'), navigate: 'Picture' },
        { id: '2', name: 'Jack Simmons', boi: 'Digital marketing executive', image: require('../assets/images/Ellipse1.png'), navigate: 'Picture' },
        { id: '3', name: 'John Doe', boi: 'Digital marketing executive', image: require('../assets/images/Ellipse3.png'), navigate: 'Picture' },
        { id: '4', name: 'Genner Shaw', boi: 'Digital marketing executive', image: require('../assets/images/Ellipse5.png'), navigate: 'Picture' },
        { id: '5', name: 'Peter Jackson', boi: 'Digital marketing executive', image: require('../assets/images/Ellipse4.png'), navigate: 'Picture' },
        { id: '6', name: 'Sammy Sinner', boi: 'Digital marketing executive', image: require('../assets/images/Ellipse2.png'), navigate: 'Picture' },
        { id: '7', name: 'John Doe', boi: 'Digital marketing executive', image: require('../assets/images/Ellipse3.png'), navigate: 'Picture' },
    ];

    // Remove duplicate names
    const uniqueClients = Array.from(new Set(ListClient.map(client => client.name)))
        .map(name => {
            return ListClient.find(client => client.name === name);
        });

    const filteredClients = uniqueClients.filter(client =>
        client.name.toLowerCase().includes(searchQuery.toLowerCase())
    );

    return (
        <View style={{ backgroundColor: "#fff", width: '100%', height: '100%', textAlign: "center" }}>
            <Header title="Home" showBackButton={true} nextScreen="Picture" />
            <View style={{ alignSelf: "center", width: '50%', marginTop: 50 }}>
                <Text style={{ color: Colors.COLOR_BLACK, fontSize: 22, fontWeight: '600', textAlign: "center", marginBottom: 20 }}>{Heading}</Text>
                <View style={styles.meet_box}>
                    <View style={styles.search}>
                        <TextInput
                            style={{ fontSize: 14, color: "#545454", paddingHorizontal: 15 }}
                            placeholder='Search whom you want to meet'
                            value={searchQuery}
                            onChangeText={text => setSearchQuery(text)}
                        />
                    </View>
                    <FlatList
                        data={filteredClients}
                        ListEmptyComponent={
                            <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', marginVertical: 50 }}>
                                <Text style={{ textAlign: 'center', color: '#C4C4C4', fontSize: 16 }}>
                                    {searchQuery ? 'No results found' : 'None available right now!'}
                                </Text>
                            </View>
                        }
                        renderItem={({ item, index }) => {
                            return (
                                <TouchableOpacity key={index} style={styles.clientlist_style}
                                    onPress={() => { navigation.navigate(item?.navigate) }}>
                                    <Image
                                        style={{ resizeMode: 'cover', width: 50, height: 50, borderRadius: 50, marginRight: 15, elevation: 1 }}
                                        source={item.image}
                                    />
                                    <View>
                                        <Text style={{ color: Colors.COLOR_BLACK, fontSize: 17, fontWeight: '600' }}>{item.name}</Text>
                                        <Text style={{ color: "#535353", fontSize: 15, fontWeight: '400', paddingTop: 0 }}>{item.boi}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        }}
                    />
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    clientlist_style: {
        flexDirection: 'row',
        marginBottom: 20
    },
    meet_box: {
        borderColor: '#D2D2D2',
        borderWidth: 1,
        borderStyle: 'solid',
        padding: 15,
        borderRadius: 4,
    },
    search: {
        borderColor: '#D2D2D2',
        borderBottomWidth: 1,
        borderStyle: 'solid',
        paddingBottom: 10,
        marginBottom: 20
    }
});

export default Client;
