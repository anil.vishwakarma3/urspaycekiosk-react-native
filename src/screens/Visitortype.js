import React from 'react'
import { Dimensions, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import Header from './Header';
import { Colors } from '../colors';
import { useNavigation } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');
const Visitortype = () => {
    const navigation = useNavigation();
    const Visitor = 'Visitor type';
    const text_visitor = 'We need to know what kind of visit you’re here for.';
    const visitprlist = [
        { id: '1', name: 'Student', navigate: 'Information' },
        { id: '2', name: 'Labour', navigate: 'Information' },
        { id: '3', name: 'VIP', navigate: 'Information' },
        { id: '4', name: 'CEO', navigate: 'Information' },
    ]
    return (
        <View style={{ backgroundColor: "#fff", width: '100%', height: '100%', textAlign: "center" }}>
            <Header title="Home" showBackButton={true} nextScreen="Visitortype" />
            <View style={{ paddingTop: 100, paddingHorizontal: 30, alignSelf: 'center', width: '60%' }}>
                <View>
                    <Text style={{ color: Colors.COLOR_BLACK, fontSize: 25, fontWeight: '600', textAlign: 'center', paddingBottom: 8 }}>{Visitor}</Text>
                    <Text style={{ color: '#868686', fontSize: 16, fontWeight: '400', textAlign: 'center' }}>{text_visitor}</Text>
                </View>
                <View style={{ width: '100%', }}>
                    <FlatList
                        style={{ marginTop: 50, width: '100%', }}
                        data={visitprlist || []}
                        ListEmptyComponent={
                            <View style={{ width: '100%', height: height * 0.50, alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={[{ color: '#000', fontSize: 20 }]} >Not found </Text>
                            </View>}
                        renderItem={({ item, index }) => {
                            return (
                                <TouchableOpacity key={index} style={styles.flrx_style}
                                    onPress={() => { navigation.navigate(item?.navigate) }}>
                                    <Text style={{ color: Colors.COLOR_BLACK, fontSize: 16, fontWeight: '600' }}>{item.name}</Text>
                                    <Image
                                        style={{ width: 20, objectFit: 'contain' }}
                                        source={require('../assets/images/right_arrow.png')} />
                                </TouchableOpacity>
                            )
                        }}
                    />
                </View>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    flrx_style: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'space-between',
        backgroundColor: '#F6F6F6',
        paddingVertical: 16,
        paddingHorizontal: 15,
        borderRadius: 4,
        marginBottom: 15,
        width: '100%',
        // margin: 'auto'
    }
})
export default Visitortype