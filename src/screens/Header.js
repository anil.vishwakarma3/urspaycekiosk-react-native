import React, { useCallback, useEffect, useState } from 'react';
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../colors';
import { useNavigation } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');
const Header = ({ title, showBackButton, nextScreen }) => {
    const navigation = useNavigation();
    const handleGoBack = () => {
        navigation.goBack();
    };
    const handleNavigate = () => {
        if (nextScreen) {
            navigation.navigate(nextScreen);
        }
    };
    return (
        <View style={styles.header_style}>
            <View style={styles.header_flex}>
                {showBackButton && (
                    <TouchableOpacity onPress={handleGoBack} style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}
                    >
                        <Image
                            style={styles.arrow}
                            source={require('../assets/images/Arrow.png')} />
                    </TouchableOpacity>
                )}

            </View>
            <View style={[styles.header_flex, { width: '35%' }]}>
                <TouchableOpacity style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <Image
                        style={{ width: '100%', objectFit: 'contain' }}
                        source={require('../assets/images/MAinLogo.png')}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.header_flex}>
                <TouchableOpacity onPress={handleNavigate} style={{ width: '100%', flexDirection: 'row', justifyContent: 'center' }}>
                    <Image
                        style={[styles.arrow, { transform: [{ rotate: '180deg' }] }]}
                        source={require('../assets/images/Arrow.png')} />
                </TouchableOpacity>
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    header_style: {
        display: 'flex',
        flexDirection: 'row',
        // alignItems: 'center',
        width: '100%',
        justifyContent: 'space-between',
        // paddingHorizontal: 100,
        paddingVertical: 20,
        // backgroundColor:'red'

    },
    header_flex: {
        width: '20%',
        // backgroundColor: 'red',
        textAlign: "center",
        flexDirection: 'row',
        alignItems: 'center',
        position: 'relative',
        top: 20
    },
    arrow: {
        width: 10,
        objectFit: 'contain',

    }
})

export default Header
