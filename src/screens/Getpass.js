import React, { useState, Component } from 'react'
import { Dimensions, FlatList, Image, Modal, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../colors';
import { useNavigation } from '@react-navigation/native';
// import Language from '../components/Language';

const { width, height } = Dimensions.get('window');

// const [lanModal, setLanModal] = useState(false)
import Logo from "../assets/svg/LOGO.svg";
import { Assets } from '../assets';

const Getpass = () => {
    const navigation = useNavigation();
    const getpass = 'Get pass';
    const userpass = 'Use pass';

    const LangList = [
        { id: '1', langName: 'English' },
        { id: '2', langName: '(Hindi) हिंदी' },
        { id: '3', langName: '(French) français' },
        { id: '4', langName: '(Arabic) عربي' },
    ]
    const [lanModal, setLangModal] = useState(false);
    const [chekModal, setCheckModal] = useState(false);
    return (
        <View style={{ height: '100%', width: '100%', position: 'relative', backgroundColor: '#fff' }}>
            <View style={{ textAlign: 'center', height: '100%' }}>
                <View style={{ position: 'absolute', top: 30, right: 30, zIndex: 5 }}>
                    <TouchableOpacity onPress={() => setLangModal(true)} >
                        <Image
                            style={{ width: 50, height: 50, borderRadius: 50 }}
                            source={require('../assets/images/language.png')} />
                    </TouchableOpacity>
                </View>

                <View style={{ width: '100%', height: '100%', position: 'relative', bottom: 50, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <Image
                        style={{ width: 500, height: 100, objectFit: 'contain', marginBottom: 50 }}
                        source={require('../assets/images/MAinLogo.png')} />
                </View>
                {/* <Logo width={120} height={40} /> */}
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', position: 'absolute', bottom: 145, zIndex: 5, left: 0, width: '100%', margin: 'auto', }}>
                    <TouchableOpacity
                        onPress={() => { navigation.navigate('Visitortype') }}
                        style={[styles.button_pass]}>
                        <Image
                            style={{ width: 60, height: 35, objectFit: 'contain' }}
                            source={require('../assets/images/pass_img1.png')} />
                        <Text style={[styles.button_text, { color: Colors.COLOR_WHITE }]}>{getpass}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => setCheckModal(true)}
                        style={[styles.button_pass, { backgroundColor: Colors.COLOR_WHITE, }]}>
                        <Image
                            style={{ width: 55, height: 60, }}
                            source={require('../assets/images/pass_img.png')} />
                        <Text style={[styles.button_text, { color: Colors.COLOR_BLACK }]}>{userpass}</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View
                style={{ backgroundColor: "#E7F0FF", width: '100%', height: 200, position: 'absolute', bottom: 0, left: 0, zIndex: -1 }}>
            </View>


            <Modal
                animationType="slide"
                transparent={true}
                visible={lanModal}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                    setLangModal(!modalVisible);
                }}>
                <View style={styles.centeredView}>
                    <View >
                        {/* <FlatList
                            data={LangList || []}
                            renderItem={({ item, index }) => {
                                return (
                                    <TouchableOpacity style={styles.land_box}>
                                        <Text style={{ color: '#000', fontSize: 15, fontWeight: '600' }}>{item.langName}</Text>
                                    </TouchableOpacity>
                                )
                            }} /> */}
                        <TouchableOpacity style={[styles.land_box, { backgroundColor: "#176CFF", }]}>
                            <Text style={[styles.lan_text, { color: '#fff' }]}>English</Text>
                            <Image
                                source={require('../assets/images/check.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.land_box}>
                            <Text style={styles.lan_text}>(Hindi) हिंदी</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.land_box}>
                            <Text style={styles.lan_text}>(French) français</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.land_box}>
                            <Text style={styles.lan_text}>(Arabic) عربي</Text>
                        </TouchableOpacity>


                    </View>
                    <TouchableOpacity
                        onPress={() => setLangModal(false)}
                        style={{ position: 'absolute', top: 25, right: 30 }}>
                        <Image source={require('../assets/images/close.png')} />
                    </TouchableOpacity>
                </View>
            </Modal>
            <Modal
                animationType="slide"
                transparent={true}
                visible={chekModal}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                    setCheckModal(!modalVisible);
                }}>
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <View>
                            <TouchableOpacity style={[styles.check_on, { backgroundColor: '#176CFF' }]}>
                                <Image style={{ width: 38, height: 38, marginRight: 20 }}
                                    source={Assets.CHECK_OUT} />
                                <Text style={{ color: Colors.COLOR_WHITE, fontSize: 20, fontWeight: '600' }}>Check-IN</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.check_on, { backgroundColor: '#C80000B5' }]}>
                                <Image style={{ width: 38, height: 38, marginRight: 20 }}
                                    source={Assets.CHECK_IN} />
                                <Text style={{ color: Colors.COLOR_WHITE, fontSize: 20, fontWeight: '600' }}>Check-Out</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <TouchableOpacity
                        onPress={() => setCheckModal(false)}
                        style={{ position: 'absolute', top: 25, right: 30 }}>
                        <Image source={require('../assets/images/close.png')} />
                    </TouchableOpacity>
                </View>
            </Modal>
        </View>
    )
}

const styles = StyleSheet.create({
    check_on: {
        backgroundColor: 'red',
        paddingHorizontal: 35,
        paddingVertical: 10,
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        height: 85,
        borderRadius: 13,
        // elevation: ,
        // justifyContent: 'flex-end',
        width: 240,
        // justifyContent: 'flex-start'
    },
    land_box: {
        backgroundColor: '#F8F8F8',
        marginBottom: 15,
        width: 450,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 15,
        borderRadius: 6,
        justifyContent: "space-between",
    },
    lan_text: {
        color: '#000', fontSize: 15, fontWeight: '600'
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000000ba'
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 35,
        alignItems: 'center',
        shadowColor: '#0000f0',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
    },
    button_pass: {
        // paddingVertical: 40,
        // paddingHorizontal: 60,
        // borderRadius: 16,
        elevation: 5,
        backgroundColor: Colors.THEME_BG_COLOR,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: "center",
        borderRadius: 12,
        marginHorizontal: 10,
        paddingHorizontal: 50,
        // width: '42%',
        height: 110
    },
    button_text: {
        fontSize: 25,
        fontWeight: '600',
        paddingLeft: 15
    }
})
export default Getpass