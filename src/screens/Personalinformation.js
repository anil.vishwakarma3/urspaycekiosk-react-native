import React, { useState } from 'react'
import { Dimensions, FlatList, Image, Modal, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import Header from './Header'
import { Colors } from '../colors'
import Tabs from '../components/Information.js/Tabs'
import Dailcode from '../components/CustomDialcode'
import CustomModal from '../components/CustomModal'
import { Assets } from '../assets'
import Icon from 'react-native-vector-icons/dist/EvilIcons';
import Ionicons from 'react-native-vector-icons/dist/Ionicons';


const { width, height } = Dimensions.get('window');
const Personalinformation = () => {
    const [modalVisible, setModalVisible] = useState(false);
    const [selectedGender, setSelectedGender] = useState(null);

    const openModal = () => {
        setModalVisible(true);
    };

    const closeModal = () => {
        setModalVisible(false);
    };

    const Heading = 'Personal information';
    const Gender = [
        { id: '1', name: 'Male' },
        { id: '2', name: 'Female' },
        { id: '3', name: 'Transgender' },
        { id: '4', name: 'Non binary' },
        { id: '5', name: 'Other' },
    ];

    const selectGender = (gender) => {
        setSelectedGender(gender);
        closeModal();
    };
    return (
        <View style={{ width: '100%', height: '100%', backgroundColor: '#fff' }}>
            <View style={styles.info_box}>
                <Text style={{ marginBottom: 30, color: Colors.COLOR_BLACK, fontSize: 25, fontWeight: '600', textAlign: 'center', }}>{Heading}</Text>
                <View style={styles.input_box}>
                    <Text style={styles.input_text}>Enter your name</Text>
                    <TextInput
                        placeholderTextColor='#717171'
                        style={styles.input}
                        placeholder="John"
                    />

                </View>
                <View style={styles.input_box}>
                    <Text style={styles.input_text}>Email</Text>
                    <TextInput
                        placeholderTextColor='#717171'
                        style={styles.input}
                        placeholder="Enter you email id"
                    />
                </View>
                {/* <View style={styles.input_box}>
                    <TextInput
                        placeholderTextColor='#717171'
                        style={styles.input}
                        placeholder="Enter you email id"
                    />
                </View> */}

                
                <View style={[styles.input_box,
                {
                    flexDirection: 'row', alignItems: 'center', borderColor: '#D2D2D2',
                    borderWidth: 1,
                    borderStyle: 'solid',
                    borderRadius: 3,
                    paddingHorizontal: 12,
                    fontSize: 13,
                    fontWeight: '400',
                    height: 48,
                    marginBottom: 40
                }]}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image
                            style={{ width: 20, height: 20, objectFit: 'contain' }}
                            source={require('../assets/images/dial-flag.png')}
                        />
                        <Text style={{ color: "#717171", fontSize: 16, fontWeight: '400', paddingHorizontal: 7 }}>+1</Text>
                        <Image
                            source={require('../assets/images/dial-arrow.png')} />
                    </View>
                    <TextInput
                        placeholder="(555) 555-1234"
                        placeholderTextColor="#717171"
                        // style={styles.input}
                        style={{
                            borderLeftWidth: 1, borderLeftColor: '#717171', paddingLeft: 15, marginLeft: 15,
                            marginVertical: 3
                        }}
                    />
                </View>
                <View style={styles.input_box}>
                    <TextInput
                        placeholderTextColor='#717171'
                        style={styles.input}
                        placeholder="Vaccination status"
                    />
                </View>
                <View style={styles.input_box}>
                    <TouchableOpacity style={[styles.input, { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }]}
                        onPress={openModal} >
                        <Text style={selectedGender ? styles.genderSelected : styles.genderPlaceholder}>
                            {selectedGender ? selectedGender.name : 'Select Gender'}
                        </Text>
                        <Icon name="chevron-down" size={35} color="#4C4C4C" />
                    </TouchableOpacity>
                </View>

            </View>

            <CustomModal visible={modalVisible} onClose={closeModal}>
                <View style={styles.row_gender}>
                    <Text></Text>
                    <Text style={{ color: Colors.COLOR_BLACK, fontSize: 20, fontWeight: '600' }}>Choose Gender</Text>
                    <TouchableOpacity onPress={() => setModalVisible(false)}>
                        <Ionicons name='close-outline' size={35} color='#000' />
                    </TouchableOpacity>
                </View>
                <FlatList
                    data={Gender || []}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                            style={[
                                styles.gender_box,
                                selectedGender && selectedGender.id === item.id ? styles.selectedGenderBox : null
                            ]}
                            onPress={() => selectGender(item)}
                        >
                            <Text style={{ color: Colors.COLOR_BLACK, fontWeight: '600', fontSize: 16 }}>{item.name}</Text>
                        </TouchableOpacity>
                    )}
                />
            </CustomModal>
        </View >
    )
}

const styles = StyleSheet.create({
    input_box: {
        marginBottom: 10,
        position: "relative"
    },
    input_text: {
        color: '#7D7D7D',
        fontWeight: '400',
        fontSize: 15,
        backgroundColor: '#fff',
        alignSelf: 'flex-start',
        position: 'absolute',
        top: -17,
        zIndex: 5,
        left: 10,
        paddingHorizontal: 5,
    },
    input: {
        borderColor: '#D2D2D2',
        borderWidth: 1,
        borderStyle: 'solid',
        borderRadius: 6,
        paddingHorizontal: 12,
        fontSize: 14,
        fontWeight: '400',
        height: 52,
        marginBottom: 20

    },
    info_box: {
        backgroundColor: '#fff',
        elevation: 3,
        // margin: 0,
        padding: 20,
        borderRadius: 12,
        marginBottom: 10
    },
    Gender_dropdown: {
        width: '100%',
        elevation: 2,
        backgroundColor: '#fff',
        marginHorizontal: 0,
        padding: 10,
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        marginTop: 5
    },
    Gender_dropdownItem: {
        borderBottomColor: '#D2D2D2',
        borderBottomWidth: 1,
        marginBottom: 7,

        paddingBottom: 7
    },
    row_gender: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 12
    },
    gender_box: {
        paddingVertical: 15,
        paddingHorizontal: 10
    },
    selectedGenderBox: {
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 1.86,
        borderColor: '#176CFF',
        shadowColor: "#176CFF",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 1,
        shadowRadius: 9.11,

        elevation: 14,
    }
})

export default Personalinformation