import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../colors'

const Footer = () => {
    return (
        <View style={styles.footer_style}>
            <Text style={{ color: Colors.COLOR_BLACK,fontSize:16,color:Colors.TEXT_GREY }}>How to pair Kiosk tablet?</Text>
            <TouchableOpacity>
                <Image
                    style={{ width: 150, objectFit: 'contain', marginLeft: 15 }}
                    source={require('../assets/images/MAinLogo.png')}
                />
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    footer_style: {
        position: 'absolute',
        bottom: 5,
        right: 0,
        flexDirection: "row",
        width: '100%',
        // paddingVertical: 25,
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingRight: 25,
    }
})

export default Footer
