import React, { useEffect } from 'react'
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Colors } from '../colors';
import { useNavigation } from '@react-navigation/native';

const { width, height } = Dimensions.get('window');
const Welcome = () => {
    const navigation = useNavigation();
    const wlecome = 'Welcome to';
    useEffect(() => {
        const timer = setTimeout(() => {
            navigation.navigate('Getpass');
        }, 3000);

        return () => clearTimeout(timer);
    }, [navigation]);
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: "center", width: '100%', height: height }}>
            <View style={{ textAlign: 'center', width: '60%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <View>
                    <Text style={{ color: Colors.COLOR_BLACK, fontSize: 35, fontWeight: '400', marginBottom: 10, textAlign: 'center' }}>{wlecome}</Text>
                    <Image
                        style={styles.images_style}
                        source={require('../assets/images/MAinLogo.png')} />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    images_style: {
        width: 500,
        height:110,
        // backgroundColor: "red",
        objectFit: 'contain'
    }
})
export default Welcome