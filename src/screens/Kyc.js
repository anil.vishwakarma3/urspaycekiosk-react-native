import React, { useState, useRef } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, ScrollView } from 'react-native';
import CustomDropdown from '../components/CustomDropdown';
import { Colors } from '../colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconRow from 'react-native-vector-icons/FontAwesome6';
import Header from './Header';
import { RNCamera } from 'react-native-camera';

const Kyc = () => {
    const Heading = 'Complete KYC';
    const [selectedValue, setSelectedValue] = useState(null);
    const [frontImage, setFrontImage] = useState(null);
    const [backImage, setBackImage] = useState(null);
    const [cameraType, setCameraType] = useState(RNCamera.Constants.Type.back);
    const [isCameraVisible, setIsCameraVisible] = useState(false);
    const [currentCapture, setCurrentCapture] = useState(null);

    const cameraRef = useRef(null);
    const options = ['license 1', 'license 2', 'license 3', 'license 4'];

    const takePicture = async () => {
        if (cameraRef.current) {
            const options = { quality: 0.5, base64: true };
            const data = await cameraRef.current.takePictureAsync(options);
            if (currentCapture === 'front') {
                setFrontImage(data.uri);
            } else {
                setBackImage(data.uri);
            }
            setIsCameraVisible(false);
        }
    };

    const handleCapture = async (captureType) => {
        setCurrentCapture(captureType);
        setIsCameraVisible(true);
        await takePicture();
    };

    const toggleCameraType = () => {
        setCameraType(prevType =>
            prevType === RNCamera.Constants.Type.back
                ? RNCamera.Constants.Type.front
                : RNCamera.Constants.Type.back
        );
    };

    return (
        <View style={{ backgroundColor: "#fff", width: '100%', height: '100%', textAlign: "center" }}>
            <ScrollView>
                <Header title="Home" showBackButton={true} nextScreen="Thank" />
                <View style={{ width: '70%', alignSelf: 'center' }}>
                    <Text style={{ color: Colors.COLOR_BLACK, fontSize: 22, fontWeight: '600', textAlign: "center", marginBottom: 30, marginTop: 50 }}>{Heading}</Text>

                    <View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 15 }}>
                            <Text style={{ color: Colors.COLOR_BLACK, fontSize: 18, fontWeight: '600' }}>1.Upload -</Text>
                            <View style={{ paddingLeft: 8, }}>
                                <CustomDropdown
                                    options={options}
                                    onSelect={(option) => setSelectedValue(option)}
                                    placeholder="*Driving license"
                                    selectedValue={selectedValue}
                                />
                            </View>
                        </View>
                        <View style={{ flexDirection: "row", flexWrap: "wrap", justifyContent: "space-between", zIndex: -1 }}>
                            <View style={styles.box_main}>
                                <Text style={{ color: '#121111', fontSize: 18, marginBottom: 20, fontWeight: '500' }}>Front Document</Text>
                                <View style={styles.box_main_border}>
                                    <View style={[styles.box_bg, { backgroundColor: '#D9D9D9' }]}>
                                        <View style={{ width: '100%', height: '100%', borderRadius: 12, overflow: 'hidden' }}>
                                            {isCameraVisible && currentCapture === 'front' ? (
                                                <RNCamera
                                                    ref={cameraRef}
                                                    style={styles.camera}
                                                    type={cameraType}
                                                    flashMode={RNCamera.Constants.FlashMode.on}
                                                    captureAudio={false}
                                                >
                                                    {/* <TouchableOpacity onPress={toggleCameraType} style={styles.toggleButton}>
                                                        <Icon name="rotate-left" size={30} color="#000" />
                                                    </TouchableOpacity> */}
                                                </RNCamera>
                                            ) : (
                                                frontImage && (
                                                    <Image source={{ uri: frontImage }} style={styles.capturedImage} />
                                                )
                                            )}
                                        </View>
                                    </View>

                                    <TouchableOpacity
                                        style={[styles.button, frontImage && styles.retakeButton]}
                                        onPress={() => handleCapture('front')}
                                    >
                                        {frontImage ? (
                                            <View style={styles.iconWithText}>
                                                <IconRow name="arrows-rotate" size={20} color="#9A8A8A" style={{ marginRight: 10 }} />
                                                <Text style={[styles.buttonText, styles.ro_text]}>Retake</Text>
                                            </View>
                                        ) : (
                                            <Text style={styles.buttonText}>Capture</Text>
                                        )}
                                    </TouchableOpacity>

                                </View>
                            </View>
                            <View style={styles.box_main}>
                                <Text style={{ color: '#121111', fontSize: 18, marginBottom: 20, fontWeight: '500' }}>Back Document</Text>
                                <View style={styles.box_main_border}>
                                    <View style={[styles.box_bg, { backgroundColor: '#D9D9D9', zIndex: -0 }]}>
                                        <View style={{ width: '100%', height: '100%', borderRadius: 12, overflow: 'hidden' }}>
                                            {isCameraVisible && currentCapture === 'back' ? (
                                                <RNCamera
                                                    ref={cameraRef}
                                                    style={styles.capturedImage}
                                                    type={cameraType}
                                                    flashMode={RNCamera.Constants.FlashMode.on}
                                                    captureAudio={false}
                                                >
                                                    {/* <TouchableOpacity onPress={toggleCameraType} style={styles.toggleButton}>
                                                        <Icon name="rotate-left" size={30} color="#000" />
                                                    </TouchableOpacity> */}
                                                </RNCamera>
                                            ) : (
                                                backImage && (
                                                    <Image source={{ uri: backImage }} style={styles.capturedImage} />
                                                )
                                            )}
                                        </View>
                                    </View>

                                    {/* <TouchableOpacity
                                        style={[styles.button, backImage && styles.retakeButton]}
                                        onPress={() => handleCapture('back')}
                                    >
                                        <Text style={[styles.buttonText, backImage && styles.ro_text]}>{backImage ? 'Retake' : 'Capture'}</Text>
                                    </TouchableOpacity> */}
                                    <TouchableOpacity
                                        style={[styles.button, backImage && styles.retakeButton]}
                                        onPress={() => handleCapture('back')}
                                    >
                                        {backImage ? (
                                            <View style={styles.iconWithText}>
                                                <IconRow name="arrows-rotate" size={20} color="#9A8A8A" style={{ marginRight: 10 }} />
                                                <Text style={[styles.buttonText, styles.ro_text]}>Retake</Text>
                                            </View>
                                        ) : (
                                            <Text style={styles.buttonText}>Capture</Text>
                                        )}
                                    </TouchableOpacity>

                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    iconWithText: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    retakeButton: {
        backgroundColor: 'transparent',
        elevation: 0,
    },
    ro_text: {
        color: '#9A8A8A',
        fontWeight: '600',
        fontSize: 18
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.COLOR_WHITE,
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    camera: {
        width: '100%',
        height: '100%',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
    toggleButtonText: {
        color: '#fff',
        fontSize: 16,
    },
    box_main: {
        flex: 1,
        margin: 10,
        minWidth: '45%', // Ensure two boxes fit side by side
    },
    box_main_border: {
        borderWidth: 1,
        borderColor: '#939393',
        borderRadius: 12,
        borderStyle: 'dotted',
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    box_bg: {
        width: '100%',
        height: 250,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
        borderRadius: 12,
    },
    capturedImage: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        zIndex: -0,
        borderRadius: 12
    },
    button: {
        backgroundColor: Colors.THEME_BG_COLOR,
        paddingVertical: 13,
        paddingHorizontal: 30,
        elevation: 5,
        borderRadius: 4,
    },
    buttonText: {
        color: '#fff',
        fontSize: 16,
    },
    toggleButton: {
        position: 'absolute',
        top: 10,
        right: 10,
        backgroundColor: 'transparent',
        padding: 10,
    },
});

export default Kyc;
