import React, { useRef, useState } from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Colors } from '../colors';
import Header from './Header';
import Footer from './Footer';
import { RNCamera } from 'react-native-camera';

const Picture = () => {
    const cameraRef = useRef(null);
    const [capturedPhoto, setCapturedPhoto] = useState(null);
    const Heading = 'Let’s get click a picture';
    const [backgroundColor, setBackgroundColor] = useState('red');
    const takePicture = async () => {
        if (cameraRef.current) {
            const options = { quality: 0.5, base64: true, mirrorImage: true };
            const data = await cameraRef.current.takePictureAsync(options);
            setCapturedPhoto(data.uri);

        }
    };

    const retakePicture = () => {
        setCapturedPhoto(null);
    };

    return (
        <View style={{ backgroundColor: "#fff", width: '100%', height: '100%', textAlign: "center" }}>
            <Header title="Home" showBackButton={true} nextScreen={capturedPhoto ? "Signature" : false} />
            <View style={{ alignSelf: "center", width: '50%', marginTop: 50 }}>
                <Text style={{ color: Colors.COLOR_BLACK, fontSize: 22, fontWeight: '600', textAlign: "center", marginBottom: 20 }}>{Heading}</Text>
                <View style={{ alignItems: "center" }}>
                    {!capturedPhoto ? (
                        <RNCamera
                            ref={cameraRef}
                            style={styles.preview}
                            type={RNCamera.Constants.Type.front}
                            flashMode={RNCamera.Constants.FlashMode.off}
                            captureAudio={false}
                        />
                    ) : (
                        <Image
                            style={styles.Picture}
                            source={{ uri: capturedPhoto }}
                        />
                    )}
                    {!capturedPhoto ? (
                        <TouchableOpacity style={styles.button} onPress={takePicture}>
                            <Text style={{ color: Colors.COLOR_WHITE, fontWeight: '500', fontSize: 20 }}>Capture</Text>
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity onPress={retakePicture}>
                            <Text style={{ color: '#9A8A8A', fontWeight: '500', fontSize: 20 }}>Retake</Text>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
            <Footer />
        </View>
    );
};

const styles = StyleSheet.create({
    preview: {
        width: 250,
        height: 250,
        borderRadius: 125,
        overflow: 'hidden',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 50,
        marginTop: 20
    },
    Picture: {
        width: 250,
        height: 250,
        borderRadius: 125,
        marginBottom: 50,
        objectFit: 'cover',
        marginTop: 20
    },
    button: {
        backgroundColor: Colors.THEME_BG_COLOR,
        paddingVertical: 20,
        paddingHorizontal: 60,
        elevation: 5,
        borderRadius: 4
    }
});

export default Picture;
