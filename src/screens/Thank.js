import React from 'react'
import { Image, Text, TouchableOpacity, View } from 'react-native'
import SVG from '../assets/svg/pass1.svg'
import Header from './Header'
import { Assets } from '../assets'
import { Colors } from '../colors'
import { useNavigation } from '@react-navigation/native'

const Thank = () => {
  const navigation = useNavigation();
  const Heading1 = 'Thank you john Your request';
  const Heading2 = 'has been sent to the Host';
  return (
    <View style={{ backgroundColor: "#fff", width: '100%', height: '100%', }}>
      <Header />
      <View style={{ alignSelf: "center", marginTop: 50, }}>
        <Image
          style={{ alignSelf: "center", marginBottom: 30 }}
          source={Assets.THNAX}
        />
        <Text style={{ color: Colors.COLOR_BLACK, fontSize: 25, fontWeight: '600', textAlign: 'center', marginTop: 5 }}>{Heading1}</Text>
        <Text style={{ color: Colors.COLOR_BLACK, fontSize: 25, fontWeight: '600', textAlign: 'center', marginTop: 5 }}>{Heading2}</Text>
        <TouchableOpacity
          onPress={() => { navigation.navigate('Home') }}
          style={{ alignSelf: 'center', backgroundColor: Colors.THEME_BG_COLOR, paddingVertical: 15, paddingHorizontal: 45, borderRadius: 5, marginTop: 50 }}>
          <Text style={{ color: Colors.COLOR_WHITE, fontSize: 15, fontWeight: '400' }}>Home</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Thank