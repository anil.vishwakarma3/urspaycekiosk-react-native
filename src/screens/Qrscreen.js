import React from 'react';
import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { useNavigation } from '@react-navigation/native';
import QrHeader from '../components/QrHeader';

const { width, height } = Dimensions.get('window');

const Qrscreen = () => {
    const navigation = useNavigation();

    const handleQRCodeRead = ({ data }) => {
        alert(`QR Code Data: ${data}`);
        navigation.navigate('Welcome');
    };

    return (
        <View style={{ backgroundColor: "#fff", width: '100%', height: '100%', textAlign: "center" }} >
            <View style={{ alignSelf: 'center' }}>
                <QRCodeScanner
                    onRead={handleQRCodeRead}
                    flashMode={RNCamera.Constants.FlashMode.off}
                    reactivateTimeout={500}
                    showMarker={true}
                    cameraStyle={styles.camera}
                    customMarker={
                        <View style={styles.markerContainer}>
                            <View style={styles.topOverlay}>
                                <QrHeader />
                            </View>
                            <View style={styles.centerContainer}>
                                <View style={styles.sideOverlay} />
                                <View style={styles.marker} />
                                <View style={styles.sideOverlay} />
                            </View>
                            <View style={styles.bottomOverlay}></View>
                        </View>
                    }
                />
            </View>
        </View>
    );
};

const overlayColor = 'rgba(0, 0, 0, 0.5)';
const styles = StyleSheet.create({
    camera: {
        height: height,
        width: '100%',
        alignItems: "center",
        alignSelf: 'center'
    },
    markerContainer: {
        flex: 1
        , flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
    },
    topOverlay: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        height: (height - 250) / 2,
        backgroundColor: overlayColor,
    },
    bottomOverlay: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: (height - 250) / 2,
        backgroundColor: overlayColor,
    },
    centerContainer: {
        flexDirection: 'row',
    },
    sideOverlay: {
        width: (width - 250) / 2,
        backgroundColor: overlayColor,
    },
    marker: {
        width: 250,
        height: 250, // Adjust this to the marker size
        borderColor: 'white',
        borderWidth: 2,
    },
});

export default Qrscreen;
