// import axios from 'axios';

// // const baseURL = 'https://bhatiagro.com/bfcoo/Api/';
// const baseURL = 'https://saafartest.com:440/api/';

// export default axiosInstance = axios.create({
//   baseURL: baseURL,
//   timeout: 5000,
//   headers: {
//     'Content-Type': 'application/json',
//     'X-API-KEY': 'development101010102020203030'
//   }
// });

import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { logOut } from '../redux/actions/action-creator';
// import { signOut } from 'redux/actions/Auth'
import { store } from '../redux/store';


export const API_BASE_URL = 'https://anomla.net/laundry/api/'

const axiosInstance = axios.create({
  baseURL: API_BASE_URL,
  headers: {
    // "Content-Type": "application/json",
    "Content-Type": "multipart/form-data",
    "X-API-KEY": "development101010102020203030",
    "lang": "en"

    // "Access-Control-Allow-Origin": "*",
  }
})

const requestHandler = request => {
  const { loginToken } = store?.getState()?.auth || '';
  const { lang = 'en' } = store?.getState()?.common || '';
  // if (loginToken != null) {
  //   request.headers['Authorization'] = `Bearer ${loginToken}`;
  // }
  request.headers['Lang'] = lang;
  return request;
};

const responseHandler = (response) => {
  const { dispatch } = store
  if (response?.status == 401 || response?.status == 403) {
    dispatch(logOut())
  }
  return response;
};


const errorHandler = error => {
  console.log("errorHandler AXIOS ------- ", error);
  if (error?.response?.status == 401 || error?.response?.status == 403) {
    responseHandler(error?.response);
  }
  return Promise.reject(error);
};

axiosInstance.interceptors.request.use(
  (request) => requestHandler(request),
  (error) => errorHandler(error)
);
axiosInstance.interceptors.response.use(
  (response) => responseHandler(response),
  (error) => errorHandler(error)
);

export default axiosInstance;