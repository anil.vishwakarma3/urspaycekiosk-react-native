import React, { useEffect, useState } from 'react'
import { Dimensions, SafeAreaView, StatusBar, Text, View } from 'react-native'
import AppContainer from './src/navigation/Index';
import SplashScreen from './src/splashScreen';
// import AppContainer from './src/navigation';

const { width, height } = Dimensions.get('window');
const App = () => {

  const [Splash, setSplash] = useState(false);
  const [progressSpeed, setProgressSpeed] = useState(0.5);

  useEffect(() => {
    onProgressChanges()
    setTimeout(() => { setSplash(true) }, 5000);
  }, [])

  const onProgressChanges = () => {
    setTimeout(() => {
      setProgressSpeed(1)
    }, 3000)
  }

  return (
    <>

      {
        !Splash
          ?
          <>
            <StatusBar hidden />
            <SplashScreen progress={progressSpeed} />
          </>
          : <>
            <SafeAreaView style={{ width: '100%', backgroundColor: '#000' }}>
              <StatusBar backgroundColor={'#000'} barStyle={'light-content'} />
            </SafeAreaView>
            <AppContainer />
          </>
      }

    </>
    // <View style={{ position: "absolute", width: '100%', height: '100%', backgroundColor: '#fff' }}>
    //   <AppContainer />
    // </View>
  )
}


export default App